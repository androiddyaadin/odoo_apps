package com.dyaadin.odooapps.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.dyaadin.odooapps.R;
import com.dyaadin.odooapps.databinding.ActivityLoginBinding;
import com.dyaadin.odooapps.framework.RequestManager;
import com.dyaadin.odooapps.framework.SessionManager;

import de.timroes.axmlrpc.XMLRPCCallback;
import de.timroes.axmlrpc.XMLRPCException;
import de.timroes.axmlrpc.XMLRPCServerException;

public class LoginActivity extends AppCompatActivity {
    private ActivityLoginBinding binding;
    private SessionManager session;
    private RequestManager request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        session = new SessionManager(this);
        request = new RequestManager(this);

        binding.editurl.setText(session.getUrl());
        binding.editdb.setText("odoo11c_mobile");
        binding.editusername.setText("admin");
        binding.editpassword.setText("Pedas007");

        binding.btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                session.setUrl(binding.editurl.getText().toString());
                String db = binding.editdb.getText().toString();
                String username = binding.editusername.getText().toString();
                String password = binding.editpassword.getText().toString();
                request.login(db, username, password, new XMLRPCCallback() {

                    @Override
                    public void onStart(long id) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                binding.container.setVisibility(View.GONE);
                                binding.loading.setVisibility(View.VISIBLE);
                            }
                        });
                    }

                    @Override
                    public void onResponse(long id, final Object result) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if(result.toString().equals("false")){
                                    Toast.makeText(LoginActivity.this, "username / password salah", Toast.LENGTH_SHORT).show();
                                }else{
                                    Toast.makeText(LoginActivity.this, "login berhasil " + result.toString(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }

                    @Override
                    public void onError(long id, XMLRPCException error) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(LoginActivity.this, "connection error, check host", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                    @Override
                    public void onServerError(long id, XMLRPCServerException error) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(LoginActivity.this, "server error, check db", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                    @Override
                    public void onFinish(long id) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                binding.loading.setVisibility(View.GONE);
                                binding.container.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                });
            }
        });
    }
}
