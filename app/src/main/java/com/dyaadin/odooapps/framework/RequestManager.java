package com.dyaadin.odooapps.framework;

import android.content.Context;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import de.timroes.axmlrpc.XMLRPCCallback;
import de.timroes.axmlrpc.XMLRPCClient;

/**
 * Created by dyaadin on 01/01/18.
 */

public class RequestManager {
    private static String COMMON_PATH = "/xmlrpc/2/common";
    private static String OBJECT_PATH = "/xmlrpc/2/object";
    private SessionManager session;
    private Context context;

    public RequestManager(Context context){
        this.session = new SessionManager(context);
        this.context = context;
    }

    public void login(String db, String username, String password, XMLRPCCallback listener){
        try {
            URL url = new URL(session.getUrl() + COMMON_PATH);
            Map<String, Object> emptyMap = new HashMap<String, Object>();
            XMLRPCClient client = new XMLRPCClient(url);
            client.callAsync(listener, "authenticate", db, username, password, emptyMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
