package com.dyaadin.odooapps.framework;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by dyaadin on 01/01/18.
 */

public class SessionManager {
    private static final String SESSION = "ODOO_APP_SESSION";
    private static final String URL = "url";
    private static final String DEFAULT_URL = "http://172.16.4.138:8086";
    private static final String DB = "db";
    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";
    private static final String UID = "uid";
    private static final int MODE = Context.MODE_PRIVATE;
    private SharedPreferences session;

    public SessionManager(Context context){
        session = context.getSharedPreferences(SESSION, MODE);
    }

    public void setUrl(String value) {
        session.edit().putString(URL, value).commit();
    }

    public String getUrl(){
        return session.getString(URL, DEFAULT_URL);
    }

    public void setDb(String value){
        session.edit().putString(DB, value).commit();
    }

    public String getDb(){
        return session.getString(DB, "");
    }

    public void setUsername(String value){
        session.edit().putString(USERNAME, value).commit();
    }

    public String getUsername(){
        return session.getString(USERNAME, "");
    }

    public void setPassword(String value){
        session.edit().putString(PASSWORD, value).commit();
    }

    public String getPassword(){
        return session.getString(PASSWORD, "");
    }

    public void setUid(String value){
        session.edit().putString(UID, value).commit();
    }

    public String getUid(){
        return session.getString(UID, "");
    }

    public void logOut(){
        this.setUrl("");
        this.setDb("");
        this.setUsername("");
        this.setPassword("");
    }
}
